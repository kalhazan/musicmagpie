jQuery(document).ready(function() {



    /* --------- Mobile menu --------- */

    jQuery('.menu-toggle').click(function() {

        if (jQuery('.navigation').hasClass('visible')) {
            jQuery('.navigation').removeClass('visible');
        } else {
            jQuery('.navigation').addClass('visible');
        }

    });

    /* --------- Mobile menu --------- */



    /* --------- Accordion --------- */

    var allAccordions = jQuery('.accordion div.accordion-data');
    var allAccordionItems = jQuery('.accordion .accordion-item');
    jQuery('.accordion > .accordion-item').click(function() {
    if(jQuery(this).hasClass('open'))
        {
            jQuery(this).removeClass('open');
            jQuery(this).next().slideUp("slow");
        }
        else
        {
            allAccordions.slideUp("slow");
            allAccordionItems.removeClass('open');
            jQuery(this).addClass('open');
            jQuery(this).next().slideDown("slow");
            return false;
        }
    });

    /* --------- Accordion --------- */



    /* --------- Add a class "active" when you hover one of blog posts --------- */

    jQuery('.post-list .post').each(function() {

        jQuery(this).find('.post__button').hover(function() {
            if (jQuery(this).parent().hasClass('active')) {
                jQuery(this).parent().removeClass('active');
            } else {
                jQuery(this).parent().addClass('active');
            }
        });

        jQuery(this).find('.post__thumbnail').hover(function() {
            if (jQuery(this).parent().hasClass('active')) {
                jQuery(this).parent().removeClass('active');
            } else {
                jQuery(this).parent().addClass('active');
            }
        });

        jQuery(this).find('.post__title a').hover(function() {
            if (jQuery(this).parent().parent().hasClass('active')) {
                jQuery(this).parent().parent().removeClass('active');
            } else {
                jQuery(this).parent().parent().addClass('active');
            }
        });

    });

    /* --------- Add a class "active" when you hover one of blog posts --------- */



    /* --------- Zoom image --------- */

    var divOffset = $(".zoom").offset();

    $(".zoom").mousemove(function(event){

        var top = event.pageY - divOffset.top;
        var left = event.pageX - divOffset.left;

        $(".zoom .zoomed").css({
            "opacity" : "1",
            "top" : "-"+top+"px",
            "left" : "-"+left+"px"
        });

    }).mouseleave(function(){
        $(".zoom .zoomed").css("opacity","0");
    });

    /* --------- Zoom image --------- */



});