$(document).ready(function() {
    var menuHandler = 0;
    $('.showSideNavButton').click(function(){
        $('.mainNavigationWrapper').css('left','-270px');
        $('.mainNavigationWrapper').css('transition','1.25s');
        $('.mainNavigationWrapper').addClass('openedMenu');
        $('.main_page ').css('transform','translateX(270px)');
        $('.showSideNavButton img').attr('src','img/close-button.png');
        menuHandler++;
        if(menuHandler % 2 == 0){
            $('.showSideNavButton img').attr('src','img/mobile-menu.png');
            $('.mainNavigationWrapper').css('left','-270px');
            $('.mainNavigationWrapper').css('transition','.25s');
            $('.main_page ').css('transform','translateX(0)');
            $('.mainNavigationWrapper').removeClass('openedMenu');
        }
    });
    $('.closeMenu').click(function(){
        $('.main_page ').css('transform','translateX(0)');
        $('.showSideNavButton img').attr('src','img/mobile-menu.png');
        $('.mainNavigationWrapper').removeClass('openedMenu');
        menuHandler++;
    });
    if($(window).width() < 990){
        $(document).mouseup(function (e){
            var menu = $(".mainNavigationWrapper");
            var menuOpener = $('.showSideNavButton');
            if (menu.hasClass('openedMenu') && !menu.is(e.target) && menu.has(e.target).length === 0 && !menuOpener.is(e.target) && menuOpener.has(e.target).length === 0) {
                $('.main_page ').css('transform','translateX(0)');
                $('.showSideNavButton img').attr('src','img/mobile-menu.png');
                $('.mainNavigationWrapper').removeClass('openedMenu');
                menuHandler++;
            }
        });
        $('.books ul').prepend("<li><a href='#' class='backToMainMenu'><img src='img/pulloutleftarrowTwo.png' alt='back'><span>Back</span></a></li>");
        $('.books .header').click(function(){
            $('.books ul').addClass('openedSubList');
            $('.mainNavigationWrapper').addClass('closedMainMenu');
        });
        $('.books .backToMainMenu').click(function(){
            $('.books ul').removeClass('openedSubList');
            $('.mainNavigationWrapper').removeClass('closedMainMenu');
        });
        $('.app ul').prepend("<li><a href='#' class='backToMainMenu'><img src='img/pulloutleftarrowTwo.png' alt='back'><span>Back</span></a></li>");
        $('.app .header').click(function(){
            $('.app ul').addClass('openedSubList');
            $('.mainNavigationWrapper').addClass('closedMainMenu');
        });
        $('.app .backToMainMenu').click(function(){
            $('.app ul').removeClass('openedSubList');
            $('.mainNavigationWrapper').removeClass('closedMainMenu');
        });
        $('.disks .closedSubList').prepend("<li><a href='#' class='backToMainMenu'><img src='img/pulloutleftarrowTwo.png' alt='back'><span>Back</span></a></li>");
        $('.disks .header').click(function(){
            $('.disks .closedSubList').addClass('openedSubList');
            $('.mainNavigationWrapper').addClass('closedMainMenu');
        });
        $('.disks .backToMainMenu').click(function(){
            $('.disks .closedSubList').removeClass('openedSubList');
            $('.mainNavigationWrapper').removeClass('closedMainMenu');
        });
        $('.phone .closedSubList').prepend("<li><a href='#' class='backToMainMenu'><img src='img/pulloutleftarrowTwo.png' alt='back'><span>Back</span></a></li>");
        $('.phone .header').click(function(){
            $('.phone .closedSubList').addClass('openedSubList');
            $('.mainNavigationWrapper').addClass('closedMainMenu');
        });
        $('.phone .backToMainMenu').click(function(){
            $('.phone .closedSubList').removeClass('openedSubList');
            $('.mainNavigationWrapper').removeClass('closedMainMenu');
        });
        $('.header-iphone').click(function(){
            $('.sellIphone').show();
            $('.sellIphone').addClass('openedSubList');
        });
        $('.sellIphone .backToMainMenu').click(function(){
            $('.sellIphone').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuPhone').addClass('openedSubList');
        });
        $('.header-nokia').click(function(){
            $('.sellNokia').show();
            $('.sellNokia').addClass('openedSubList');
        });
        $('.sellNokia .backToMainMenu').click(function(){
            $('.sellNokia').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuPhone').addClass('openedSubList');
        });
        $('.header-samsung').click(function(){
            $('.sellSamsung').show();
            $('.sellSamsung').addClass('openedSubList');
        });
        $('.sellSamsung .backToMainMenu').click(function(){
            $('.sellSamsung').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuPhone').addClass('openedSubList');
        });
        $('.header-galaxy-s').click(function(){
            $('.sellGalaxy').show();
            $('.sellGalaxy').addClass('openedSubList');
        });
        $('.sellGalaxy .backToMainMenu').click(function(){
            $('.sellGalaxy').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.sellSamsung').addClass('openedSubList');
            $('.sellSamsung').show();
        });
        $('.header-galaxy-note').click(function(){
            $('.sellNote').show();
            $('.sellNote').addClass('openedSubList');
        });
        $('.sellNote .backToMainMenu').click(function(){
            $('.sellNote').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.sellSamsung').addClass('openedSubList');
            $('.sellSamsung').show();
        });
        $('.tech .closedSubList').prepend("<li><a href='#' class='backToMainMenu'><img src='img/pulloutleftarrowTwo.png' alt='back'><span>Back</span></a></li>");
        $('.tech .header').click(function(){
            $('.tech .closedSubList').addClass('openedSubList');
            $('.mainNavigationWrapper').addClass('closedMainMenu');
        });
        $('.tech .backToMainMenu').click(function(){
            $('.tech .closedSubList').removeClass('openedSubList');
            $('.mainNavigationWrapper').removeClass('closedMainMenu');
        });
        $('.header-tech').click(function(){
            $('.sellTech').show();
            $('.sellTech').addClass('openedSubList');
        });
        $('.sellTech .backToMainMenu').click(function(){
            $('.sellTech').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuTech').addClass('openedSubList');
        });
        $('.header-tech').click(function(){
            $('.sellTech').show();
            $('.sellTech').addClass('openedSubList');
        });
        $('.sellTech .backToMainMenu').click(function(){
            $('.sellTech').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuTech').addClass('openedSubList');
        });
        $('.header-consoles').click(function(){
            $('.sellConsoles').show();
            $('.sellConsoles').addClass('openedSubList');
        });
        $('.sellConsoles .backToMainMenu').click(function(){
            $('.sellConsoles').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.subMenuTech').addClass('openedSubList');
        });
        $('.header-apple-ipad').click(function(){
            $('.sellIpad').show();
            $('.sellIpad').addClass('openedSubList');
        });
        $('.sellIpad .backToMainMenu').click(function(){
            $('.sellIpad').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.sellTech').show();
            $('.sellTech').addClass('openedSubList');
        });
        $('.header-apple-iphone').click(function(){
            $('.sellAppleIphone').show();
            $('.sellAppleIphone').addClass('openedSubList');
        });
        $('.sellAppleIphone .backToMainMenu').click(function(){
            $('.sellAppleIphone').hide();
            $('.mainNavigationWrapper').addClass('closedMainMenu');
            $('.sellTech').show();
            $('.sellTech').addClass('openedSubList');
        });
    }
});