var config = {
    src: './src/',
    dest: './src/',
    html: {
        src: '*.html'
    },
    css: {
        dest: 'css'
    },
    sass: {
        watch: 'sass/**/*.scss',
        src: 'sass/style.scss'
    }
};

var gulp = require('gulp');
var sass = require('gulp-sass');
var gcmq = require('gulp-group-css-media-queries');
var autoprefixer = require('gulp-autoprefixer');
var clean = require('gulp-clean-css');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();
var watch = require('gulp-watch');
var prefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var cssmin = require('gulp-minify-css');
var reload = browserSync.reload;

gulp.task('build', function(){
    gulp.src(config.src + config.css.dest + '**/*.css')
        .pipe(autoprefixer({
            browsers: ['> 0.001%'],
            cascade: false
        }))
        .pipe(clean())
        .pipe(rename({suffix: '.min', prefix : ''}))
        .pipe(gulp.dest(config.src));
});

gulp.task('sass', function(){
    gulp.src(config.src + config.sass.src) 
        .pipe(sass())
        .pipe(gcmq())
        .pipe(autoprefixer({
            browsers: ['> 0.001%'],
            cascade: false
        }))
        /*.pipe(clean())*/
        .pipe(gulp.dest(config.src + config.css.dest))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('html', function(){
    gulp.src(config.src + config.html.src)
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browserSynh', function(){
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    })
});

gulp.task('watch', ['browserSynh'], function(){
    gulp.watch(config.src + config.sass.watch, ['sass']);
    gulp.watch(config.src + config.html.src, ['html']);
});